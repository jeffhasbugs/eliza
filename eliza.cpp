#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <queue>
#include <map>
#include <utility>
#include <memory>
#include <fstream>
#include <stdexcept>

//////  //       //  ////////      ///
//      //       //       //     //  //  
/////   //       //     //      ////////
//      //       //   //       //     // 
//////  //////   //  ////////  //     //    

// ELIZA
// This program intends to simulate the ELIZA program
// specified in the original paper.
//
// The basic idea 
// Keyword database: Map: [keyword], [vector of rules]
//     e.g. [please], [("Can you 0"), ("I will 0")]
// (*) Keyword ranking with map<string, int>
// (*) Cycling between reassembly rules: Map: [keyword], [position]
// ( ) Shared rules for keywords  (=WHY)
// ( ) TAGGING   DLIST(/family)
//
// (*) KEYSTACK
// Keywords found for a given input shall be put into a keystack.
// New keywords with higher rank than the keyword at the top of the
// stack will  be placed at the top, otherwise it will be placed at
// the bottom.
//
// The decomposer rules are thereafter tried until a suitable match
// is found.
//
// Requires that the keyword be stored with the transformation
// in the keystack.
//
// ( ) SCRIPT PARSER
// ( ) Unconditional substitutions (during scanning) - separate during "level 1" parsing
//
// ( ) MEMORY
// The program "remembers" facts given by the user with the
// my keyword. A trasnformation is applied and stored at a
// first-in-first-out list.

typedef std::shared_ptr<std::vector<std::string>> TransformationPtr;
typedef std::map<std::string, std::vector<TransformationPtr>> KeyDatabase;
typedef std::list<std::pair<std::string, std::vector<TransformationPtr>>> KeyStack;

// function declarations
KeyStack build_keystack(const KeyDatabase&, const std::map<std::string, int>&, const std::string&);
bool validate_decomposer(const std::string&, const std::string&);
std::vector<std::string> decompose(TransformationPtr, const std::string&);
KeyStack::const_iterator find_decomposer(KeyStack::const_iterator, KeyStack::const_iterator,
                                         const std::string&);
std::string reassemble(TransformationPtr, const std::vector<std::string>&);
KeyDatabase parse(std::ifstream);
 
KeyStack build_keystack(const KeyDatabase &keywords,
                        const std::map<std::string, int> &keyrank,
                        const std::string &input)
{
    KeyStack keystack;
    std::istringstream ss(input);
    std::string word = "";
    int highest_rank = 0; // in the keystack
    while (ss >> word) {
        auto findresult = keywords.find(word);
        if (findresult != keywords.end()) { 
            auto pair = std::make_pair(findresult->first, findresult->second);
            int rank = keyrank.find(word)->second;
            if (rank > highest_rank) {
                keystack.push_front(pair);
                highest_rank = rank;
            }
            else {
                keystack.push_back(pair);
            }
        }
    }
    keystack.push_back( std::make_pair("NONE", keywords.find("NONE")->second) );
    return keystack;
}

bool validate_decomposer(const std::string &decomposer, const std::string &input) 
{
    std::istringstream ss1(input);
    std::istringstream ss2(decomposer);
    
    std::string input_w, curr;
    bool zero = false;
    while (ss2 >> curr) {
        if (curr == "0") {
            zero = true;
            ss2 >> curr;
        }
        // if checks fail here, return immediately
        while (ss1 >> input_w) {
            if (input_w != curr) {
                if (!zero) 
                    return false;
            }
            else
                zero = false;
        }
    }
    return true;
}

std::vector<std::string> decompose(TransformationPtr ptr, const std::string &input)
{
    std::string rule = ptr->front();
    std::vector<std::string> v;
    std::istringstream ss1(input);
    std::istringstream ss2(rule);

    std::string word, curr, next;
    std::string buffer;

    while (ss2 >> curr) {
        next = curr;
        if (curr == "0") {
            // in case ss2 is empty
            next = " \\n";
            ss2 >> next;
        }
        
        buffer = "";
        while (ss1 >> word) {
            if (word == next) {
                v.push_back(buffer);
                v.push_back(next);
                break;
            }
            else 
                buffer += word + " ";
        }
    }
    return v;
}

// traverse the keystack and find a suitable decomposer tied to a 
// keyword with the highest rank possible
TransformationPtr find_decomposer(const KeyStack &keystack, const std::string &input)
{
    // list{vector{TransformationPtr}}
    for (auto it = keystack.cbegin(); it != keystack.cend(); ++it) {
        auto &word_data = it->second;
        for (auto it2 = word_data.cbegin(); it2 != word_data.cend(); ++it2) {
            // vector -> TransformationPtr -> vector
            TransformationPtr ptr = *it2;
            std::string decomposer = ptr->front();
            if (validate_decomposer(decomposer, input))
                return ptr;
        }
        
    }
    return nullptr;
}

std::string reassemble(TransformationPtr transformation, std::vector<std::string> &decomposition)
{
    // the index of the transformation to use
    std::string s_index = (*transformation)[1];
    // corrections for positioning and 0-indexing
    int index = std::stoi(s_index) + 2 - 1;
    
    // the reassembly rule to use
    const std::string &rule = (*transformation)[index];
    std::istringstream ss(rule);
    std::string word = "";
    
    std::string result = "";
    while (ss >> word) {
        if (word.find_first_not_of("0123456789") == std::string::npos)
            result += decomposition[std::stoi(word)] ;
        else
            result += word ;
        result += " ";
    }
    
    std::cout << result << std::endl;
    return result;
}

// Because the same word rule in a script can span multiple lines,
// interpreting the text as we parse is not doable. The best parse()
// can do is to turn the input text into a vector for use later.
// pure text -> structured vector -> put to use
std::shared_ptr<std::vector<std::vector<std::string>>> parse(const std::list<std::string> &lines) {
{
    std::shared_ptr<std::vector<std::vector<std::string>>>
        ptr = std::make_shared<std::vecotr<std::vector<std::string>>>();
    int parantheses_level = 0;
    for (std::size_t line_no = 0; line_no < lines.size(); ++line_no) {
        std::string buffer = "";
        for (char c : lines[line_no]) {
            if (c == '(') {
                parantheses_level += 1;
                if (parantheses_level == 4) {
                    std::string message = "Parsed " + parantheses_level + " levels of parantheses "
                                          "at line " + line_no + ". Maximum depth is 3.";
                    throw std::runtime_error(message);
                }
            }
            else if (c == ')') {
                parantheses_level -= 1;
                switch (parantheses_level) {
                    case 1:
                        vec[0].push_back(buffer);
                        break;
                    case 2:
                        vec[1].push_back(buffer);
                        break;
                    case 3:
                        vec[2].push_back(buffer);
                        break;
                }
            }
            else if (c == ' ') {
                words.push_back(buffer);
                buffer = "";
            }
            else buffer += c;
        }
    }
}

int main() {
    // we will use a list for storing the script
    // because list supports pop_front() and we don't 
    // need the fast random access provided by vector
    // during parsing.
    std::list<std::string> script;
    std::ifstream in("script.txt");
    for (std::string line = ""; std::getline(in, line);) {
        // ignore comments
        if (line[0] == ';') continue;
        script.push_back(line);
    }
    in.close();

    std::string introduction = script.front(); script.pop_front();

    std::shared_ptr<KeyDatabase> keywords = parse(script);
    std::map<std::string, int> keyranks;
    std::string input; std::getline(std::cin, input);

    KeyStack keystack = build_keystack(keywords, keyranks, input);

    TransformationPtr ptr = find_decomposer(keystack, input);

    std::vector<std::string> decomposition = decompose(ptr, input);

    std::string output = reassemble(ptr, decomposition);
    std::cout << output << std::endl; 
}
